package com.mmhdev.privatechat.ui.screens.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.mmhdev.privatechat.R;
import com.mmhdev.privatechat.core.android.App;
import com.mmhdev.privatechat.core.android.BaseActivity;
import com.mmhdev.privatechat.ui.di.AuthComponent;

import butterknife.ButterKnife;

/**
 * Created by on 25.11.16.
 */

public class AuthActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        ButterKnife.bind(this);
        setupDagger();
    }

    private void setupDagger(){
        App.getApp(this)
                .getAppComponent()
                .plus(new AuthComponent.Module())
                .inject(this);
    }
}
