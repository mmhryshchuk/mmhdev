package com.mmhdev.privatechat.ui.screens.auth.login;

import com.mmhdev.privatechat.core.mvp.BasePresenter;

/**
 * Created by on 25.11.16.
 */

public class LoginPresenterImpl extends BasePresenter<LoginView> implements LoginPresenter {

    public LoginPresenterImpl() {
    }

    @Override
    protected void onViewAttached() {
        super.onViewAttached();
    }
}
