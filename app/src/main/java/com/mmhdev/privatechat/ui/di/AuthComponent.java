package com.mmhdev.privatechat.ui.di;

import com.mmhdev.privatechat.core.di.scope.PerActivity;
import com.mmhdev.privatechat.ui.screens.auth.AuthActivity;
import com.mmhdev.privatechat.ui.screens.auth.login.LoginPresenter;
import com.mmhdev.privatechat.ui.screens.auth.login.LoginPresenterImpl;

import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;

/**
 * Created by on 25.11.16.
 */

@Subcomponent(modules = AuthComponent.Module.class)
public interface AuthComponent {

    void inject(AuthActivity authActivity);


    @dagger.Module
    class Module{

        @Provides
        @PerActivity
        LoginPresenter provideLoginPresenter(){
            return new LoginPresenterImpl();
        }
    }
}
