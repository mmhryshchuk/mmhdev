package com.mmhdev.privatechat.ui.screens.auth.login;

import com.mmhdev.privatechat.core.mvp.Presenter;

/**
 * Created by on 25.11.16.
 */

public interface LoginPresenter extends Presenter<LoginView> {
}
