package com.mmhdev.privatechat.ui.screens.auth.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mmhdev.privatechat.R;
import com.mmhdev.privatechat.core.mvp.ViewMvpFragment;

import butterknife.ButterKnife;

/**
 * Created by on 25.11.16.
 */

public class LoginFragment extends ViewMvpFragment<LoginPresenter> implements LoginView {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment,container,false);
        ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getPresenter().attachView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        getPresenter().detachView();
    }
}
