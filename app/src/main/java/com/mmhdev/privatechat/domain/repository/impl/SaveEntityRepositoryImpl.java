package com.mmhdev.privatechat.domain.repository.impl;


import com.annimon.stream.Stream;
import com.mmhdev.privatechat.core.utils.StringUtils;
import com.mmhdev.privatechat.domain.repository.SaveEntityRepository;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by vladimir on 08.06.16.
 */
public class SaveEntityRepositoryImpl implements SaveEntityRepository {

 /*   private DaoSession daoSession;
    private CatalogueMapper catalogueMapper;
    private PurchaseIssueMapper purchaseIssueMapper;
    private SubscriptionMapper subscriptionMapper;

    public SaveEntityRepositoryImpl(
            DaoSession daoSession,
            CatalogueMapper catalogueMapper,
            PurchaseIssueMapper purchaseIssueMapper,
            SubscriptionMapper subscriptionMapper

    ) {
        this.daoSession = daoSession;
        this.catalogueMapper = catalogueMapper;
        this.purchaseIssueMapper = purchaseIssueMapper;
        this.subscriptionMapper = subscriptionMapper;
    }

    @Override
    public void saveCatalogueAndRelatedData(CatalogueDto dto) {
        if (dto == null) return;

        daoSession.runInTx(() -> {
            if (!StringUtils.isNullEmpty(dto.getDeleted()) && !dto.getDeleted().equals("0")){
                checkForDelete(dto);
                return;
            }

            persistsCataloguesData(dto);
            Catalogue catalogue = catalogueMapper.map(dto);
            daoSession.getCatalogueDao().insertOrReplace(catalogue);
        });
    }

    @Override
    public void saveIssueData(IssueDtoResponse dtoResponse) {
        if (dtoResponse == null || dtoResponse.getIssue() == null) return;

        daoSession.runInTx(() -> {
            if (!StringUtils.isNullEmpty(dtoResponse.getIssue().getDeleted()) && !dtoResponse.getIssue().getDeleted().equals("0")) {
                checkForDelete(dtoResponse.getIssue());
                return;
            }

            persistIssueData(dtoResponse);
            saveIssue(dtoResponse);
        });


    }

    @Override
    public void savePurchasedIssue(PurchasedIssueDto dto) {
        daoSession.getPurchasedIssueDao()
                .insertOrReplaceInTx(purchaseIssueMapper.map(dto));
    }

    @Override
    public void saveSubscription(SubscriptionDto dto) {
        daoSession.getSubscriptionDao()
                .insertOrReplaceInTx(subscriptionMapper.map(dto));
    }

    private void persistsCataloguesData(CatalogueDto dto){
        daoSession.getCatalogueImageDao()
                .queryBuilder()
                .where(CatalogueImageDao.Properties.CatalogueId.eq(dto.getId()))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
        daoSession.getCataloguePreviewImageDao()
                .queryBuilder()
                .where(CataloguePreviewImageDao.Properties.CatalogueId.eq(dto.getId()))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();

        if (dto.getImages() != null){
            List<CatalogueImage> catalogueImages = new ArrayList<>(dto.getImages().size());
            Stream.of(dto.getImages()).forEach(s -> {
                CatalogueImage image = new CatalogueImage();
                image.setCatalogueId(dto.getId());
                image.setUrl(s);
                catalogueImages.add(image);
            });
            daoSession.getCatalogueImageDao().insertInTx(catalogueImages);
        }

        if (dto.getPreviewImages() != null){
            List<CataloguePreviewImage> cataloguePreviewImages = new ArrayList<>(dto.getPreviewImages().size());
            Stream.of(dto.getPreviewImages()).forEach(s -> {
                CataloguePreviewImage image = new CataloguePreviewImage();
                image.setCatalogueId(dto.getId());
                image.setUrl(s);
                cataloguePreviewImages.add(image);
            });
            daoSession.getCataloguePreviewImageDao().insertInTx(cataloguePreviewImages);
        }
    }

    private void checkForDelete(CatalogueDto dto){
        daoSession.getCatalogueDao().deleteByKey(dto.getId());
        daoSession.getCatalogueImageDao().queryBuilder().where(CatalogueImageDao.Properties.CatalogueId.eq(dto.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
        daoSession.getCatalogueImageDao().queryBuilder().where(CataloguePreviewImageDao.Properties.CatalogueId.eq(dto.getId())).buildDelete().executeDeleteWithoutDetachingEntities();
    }

    private void persistIssueData(IssueDtoResponse dtoResponse){
        saveArticles(dtoResponse);
        saveArticleParagraph(dtoResponse);
        savePage(dtoResponse);
    }

    private void saveArticles(IssueDtoResponse dtoResponse){
        daoSession.getIssueArticlesDao()
            .queryBuilder()
            .where(IssueArticlesDao.Properties.Issue_id.eq(dtoResponse.getIssue().getId()))
            .buildDelete()
            .executeDeleteWithoutDetachingEntities();

        if (dtoResponse.getData().getArticles() != null){
            List<IssueArticles> issueArticles = new ArrayList<>(dtoResponse.getData().getArticles().size());
            Stream.of(dtoResponse.getData().getArticles().keySet()).forEach((s) -> {
               IssueArticles articles = new IssueArticles();
                articles.setArticleKey(s);
                articles.setIssueId(dtoResponse.getIssue().getId());
                articles.setPage(dtoResponse.getData().getArticles().get(s).getPage());
                articles.setTitle(dtoResponse.getData().getArticles().get(s).getTitle());
                issueArticles.add(articles);
            });
            daoSession.getIssueArticlesDao().insertInTx(issueArticles);
        }
    }

    private void saveArticleParagraph(IssueDtoResponse dtoResponse){
        daoSession.getIssueArticleParagraphDao()
                .queryBuilder()
                .where(IssueArticleParagraphDao.Properties.Issue_id.eq(dtoResponse.getIssue().getId()))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();

        if (dtoResponse.getData().getArticles() != null) {
            List<IssueArticleParagraph> issueArticleParagraphs = new ArrayList<IssueArticleParagraph>(dtoResponse.getData().getArticles().values().size());
            Stream.of(dtoResponse.getData().getArticles()).forEach((s) -> Stream.of(s.getValue().getObjects()).forEach(issueArticlesParagraphDto -> {
                IssueArticleParagraph paragraph = new IssueArticleParagraph();
                String url = dtoResponse.getData().getImages().getMedia().replace("%src%","");
                paragraph.setIssueId(dtoResponse.getIssue().getId());
                paragraph.setArticleId(s.getKey());
                paragraph.setParagraphKey(issueArticlesParagraphDto.getKey());
                paragraph.setTitle(issueArticlesParagraphDto.getValue().getTitle());
                paragraph.setSrc(url+issueArticlesParagraphDto.getValue().getSrc());
                paragraph.setText(issueArticlesParagraphDto.getValue().getText());
                paragraph.setType(issueArticlesParagraphDto.getValue().getType());
                paragraph.setUri(issueArticlesParagraphDto.getValue().getUri());
                issueArticleParagraphs.add(paragraph);
            }));
            daoSession.getIssueArticleParagraphDao().insertInTx(issueArticleParagraphs);

        }
    }

    private void savePage(IssueDtoResponse dtoResponse){
        daoSession.getIssuePageDao()
                .queryBuilder()
                .where(IssuePageDao.Properties.Issue_id.eq(dtoResponse.getIssue().getId()))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();

        if (dtoResponse.getData().getPageOrder() != null){
            List<IssuePage> issuePages = new ArrayList<>(dtoResponse.getData().getPageOrder().size());
            String url = dtoResponse.getData().getImages().getPage();
            Stream.of(dtoResponse.getData().getPageOrder()).forEach((s) -> {
                IssuePage page = new IssuePage();
                page.setIssue_id(dtoResponse.getIssue().getId());
                page.setPosition(dtoResponse.getData().getPageOrder().indexOf(s));
                page.setUrl(url.replace("%page%",s.toString()));
                issuePages.add(page);
            });

            daoSession.getIssuePageDao().insertInTx(issuePages);
        }
    }

    private void saveIssue(IssueDtoResponse dtoResponse){
        Catalogue catalogueNew = catalogueMapper.map(dtoResponse.getIssue());
        Catalogue catalogueOld = daoSession.getCatalogueDao().queryBuilder().where(CatalogueDao.Properties.Id.eq(dtoResponse.getIssue().getId())).unique();
        daoSession.getCatalogueDao().insertOrReplace(catalogueMapper.saveIssue(catalogueOld,catalogueNew));
    }*/
}
