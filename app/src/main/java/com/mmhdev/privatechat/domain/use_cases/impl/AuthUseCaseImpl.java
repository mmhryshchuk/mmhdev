package com.mmhdev.privatechat.domain.use_cases.impl;

import com.mmhdev.privatechat.core.android.AuthRetrofit;
import com.mmhdev.privatechat.core.executors.PostExecutionThread;
import com.mmhdev.privatechat.core.executors.ThreadExecutor;
import com.mmhdev.privatechat.domain.repository.PreferenceRepository;
import com.mmhdev.privatechat.domain.repository.SaveEntityRepository;
import com.mmhdev.privatechat.domain.use_cases.AuthUseCase;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import rx.schedulers.Schedulers;

/**
 * Created by on 22.10.16.
 */

public class AuthUseCaseImpl implements AuthUseCase {

    /*private ThreadExecutor threadExecutor;
    private PostExecutionThread postExecutionThread;
    private AuthApi authApi;
    private CatalogueApi catalogueApi;
    private PreferenceRepository preferenceRepository;
    private SaveEntityRepository saveEntityRepository;
    private ErrorMapper errorMapper;
    private DaoSession daoSession;

    public AuthUseCaseImpl(
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread,
            Retrofit retrofit,
            AuthRetrofit authRetrofit,
            PreferenceRepository preferenceRepository,
            SaveEntityRepository saveEntityRepository,
            DaoSession daoSession,
            ErrorMapper errorMapper
    ) {
        this.daoSession = daoSession;
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
        this.authApi = retrofit.create(AuthApi.class);
        this.catalogueApi = authRetrofit.create(CatalogueApi.class);
        this.preferenceRepository = preferenceRepository;
        this.saveEntityRepository = saveEntityRepository;
        this.errorMapper = errorMapper;
    }

    @Override
    public Observable<LoginDvo> login(String username, String password) {
        return authApi.login(new LoginRequest(username,password))
                .doOnNext(loginDtoResponse -> {
                    preferenceRepository.saveAccessToken(loginDtoResponse.getAuth());
                    if (loginDtoResponse.getUser() != null){
                        preferenceRepository.saveUserEmail(loginDtoResponse.getUser().getEmail());
                        preferenceRepository.saveUserImage(loginDtoResponse.getUser().getImage());
                        preferenceRepository.saveUserId(loginDtoResponse.getUser().getId());
                        preferenceRepository.saveUserName(loginDtoResponse.getUser().getName());
                    }
                })
                .flatMap(loginDtoResponse -> catalogueApi.getMyCatalogue(new MyDtoRequest(preferenceRepository.getAccessToken())))
                .doOnNext(purchasedDtoResponse -> {
                    if (purchasedDtoResponse != null){
                        daoSession.runInTx(() -> {
                            for (PurchasedIssueDto dto : purchasedDtoResponse.getIssues()){
                                saveEntityRepository.savePurchasedIssue(dto);
                            }
                            for (PurchasedIssueDto dto : purchasedDtoResponse.getIssues()){
                                saveEntityRepository.savePurchasedIssue(dto);
                            }
                        });
                    }
                })
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof HttpException) {
                        HttpException ex = (HttpException) throwable;
                        ErrorDto errorsDto = errorMapper.getErrorFromResponse(ex);
                        if (errorsDto != null) {
                            return Observable.error(new BaseException(errorsDto.getError()));
                        }
                    }
                    return Observable.error(throwable);
                })
                .map(loginDtoResponse -> new LoginDvo(username,password))
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());
    }

    @Override
    public Observable<RegisterDvo> register(String username, String password, String email) {
        return authApi.register(new RegistrationRequest(email,password,username))
                .doOnNext(loginDtoResponse -> {
                    preferenceRepository.saveAccessToken(loginDtoResponse.getAuth());
                    if (loginDtoResponse.getUser() != null){
                        preferenceRepository.saveUserEmail(loginDtoResponse.getUser().getEmail());
                        preferenceRepository.saveUserImage(loginDtoResponse.getUser().getImage());
                        preferenceRepository.saveUserId(loginDtoResponse.getUser().getId());
                        preferenceRepository.saveUserName(loginDtoResponse.getUser().getName());
                    }
                })
                .onErrorResumeNext(throwable -> {
                    if (throwable instanceof HttpException) {
                        HttpException ex = (HttpException) throwable;
                        ErrorDto errorsDto = errorMapper.getErrorFromResponse(ex);
                        if (errorsDto != null) {
                            return Observable.error(new BaseException(errorsDto.getError()));
                        }
                    }
                    return Observable.error(throwable);
                })
                .map(registerDtoResponse -> new RegisterDvo(email,username,password))
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler());
    }

    @Override
    public Observable<Object> logout() {
        preferenceRepository.clear();
        return Observable.concat(
                Observable.just(new String()),
                authApi.logout()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler())
        );
    }*/
}
