package com.mmhdev.privatechat.domain.exceptions;

/**
 * Created by on 10.10.16.
 */

public class BaseException extends Exception {

    public BaseException(String message) {
        super(message);
    }
}
