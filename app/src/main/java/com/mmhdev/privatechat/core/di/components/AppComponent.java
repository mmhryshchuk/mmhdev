package com.mmhdev.privatechat.core.di.components;

import com.mmhdev.privatechat.core.android.App;
import com.mmhdev.privatechat.core.android.AuthRetrofit;
import com.mmhdev.privatechat.core.bus.Bus;
import com.mmhdev.privatechat.core.di.modules.ApiModule;
import com.mmhdev.privatechat.core.di.modules.AppModule;
import com.mmhdev.privatechat.core.di.modules.DataModule;
import com.mmhdev.privatechat.core.di.modules.MappersModule;
import com.mmhdev.privatechat.core.di.modules.RepositoriesModule;
import com.mmhdev.privatechat.core.di.modules.ThreadExecutorsModule;
import com.mmhdev.privatechat.core.di.modules.UseCaseModule;
import com.mmhdev.privatechat.core.executors.PostExecutionThread;
import com.mmhdev.privatechat.core.executors.ThreadExecutor;
import com.mmhdev.privatechat.domain.repository.PreferenceRepository;
import com.mmhdev.privatechat.ui.di.AuthComponent;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by vladimir on 25.07.16.
 */
@Singleton
@Component(modules = {
        AppModule.class,
        DataModule.class,
        ThreadExecutorsModule.class,
        ApiModule.class,
        UseCaseModule.class,
        RepositoriesModule.class,
        MappersModule.class
})
public interface AppComponent {

    AuthComponent plus(AuthComponent.Module module);

    App getApp();
    Bus getBus();
    AuthRetrofit getAuthRetorfit();
    PreferenceRepository getPreferenceRopository();
    ThreadExecutor getJobExcecutor();
    PostExecutionThread getUIThread();

}
