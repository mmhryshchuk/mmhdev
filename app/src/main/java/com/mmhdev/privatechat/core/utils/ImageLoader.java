package com.mmhdev.privatechat.core.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import com.mmhdev.privatechat.R;

import java.io.File;

/**
 * Created by vladimir on 15.06.16.
 */
public class ImageLoader {


    public static void load(final ImageView imageView, String path,boolean crop){
        if (!crop){
            loadImage(get(imageView.getContext()), path, R.mipmap.ic_launcher, imageView);
        }else {
            loadImageWithCrop(get(imageView.getContext()), path, R.drawable.placeholder, imageView);
        }
    }

    public static void load(final SubsamplingScaleImageView imageView, String path, boolean crop){
        loadImageWithCrop(get(imageView.getContext()), path, R.drawable.placeholder, imageView);
    }

    public static void loadFromFile(final SubsamplingScaleImageView imageView, String filePath){
        Point dim = DimensionUtils.getScreenDimentions(imageView.getContext());
        boolean isTablet = LandscapeUtils.isTablet(imageView.getContext());

        imageView.setImage(ImageSource.uri(Uri.fromFile(new File(filePath)))   /*BitmapHelper.decodeSampledBitmapFromFile(
            filePath,
                dim.x * (isTablet ? 1 : 2),
                dim.y * (isTablet ? 2 : 2))*/);
    }

    public static Picasso get(Context context){
        Picasso picasso =  Picasso.with(context);
        picasso.setLoggingEnabled(false);
        return picasso;
    }

    private static void loadImage(final Picasso picasso, final String path, final int plaseholder, final ImageView imageView){
        if (StringUtils.isNullEmpty(path)){
            loadRes(imageView, plaseholder);
            return;
        }


        picasso.load(path)
                .fit()
                .noFade()
                .centerCrop()
                .placeholder(plaseholder)
                .error(plaseholder)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        picasso.load(path)
                                .fit()
                                .centerCrop()
                                .placeholder(plaseholder)
                                .error(plaseholder)
                                .into(imageView);
                    }
                });
    }

    private static void loadImageWithCrop(final Picasso picasso, final String path, final int plaseholder, final ImageView imageView){
        if (StringUtils.isNullEmpty(path)){
            loadRes(imageView, plaseholder);
            return;
        }

        imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                picasso.load(path)
                        .fit()
//                        .resize(imageView.getMeasuredWidth(), imageView.getMeasuredHeight())
                        .noFade()
                        .centerInside()
                        .placeholder(plaseholder)
                        .error(plaseholder)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                picasso.load(path)
                                        .fit()
                                        .centerCrop()
                                        .placeholder(plaseholder)
                                        .error(plaseholder)
                                        .into(imageView);
                            }
                        });
                return true;
            }
        });
    }


    private static void loadImageWithCrop(final Picasso picasso, final String path, final int plaseholder, final SubsamplingScaleImageView imageView){
        if (StringUtils.isNullEmpty(path)){

            return;
        }

        imageView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                picasso.load(path)
                        .fit()
//                        .resize(imageView.getMeasuredWidth(), imageView.getMeasuredHeight())
                        .noFade()
                        .centerInside()
                        .placeholder(plaseholder)
                        .error(plaseholder)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                imageView.setImage(ImageSource.bitmap(bitmap));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                                picasso.load(path)
                                        .fit()
                                        .noFade()
                                        .centerInside()
                                        .placeholder(plaseholder)
                                        .error(plaseholder)
                                        .into(new Target() {
                                            @Override
                                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                                imageView.setImage(ImageSource.bitmap(bitmap));

                                            }

                                            @Override
                                            public void onBitmapFailed(Drawable errorDrawable) {

                                            }

                                            @Override
                                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                                            }
                                        });
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });
                return true;
            }
        });
    }

    public static void loadRes( ImageView imageView, int res){
        get(imageView.getContext()).load(res).into(imageView);
    }


}
