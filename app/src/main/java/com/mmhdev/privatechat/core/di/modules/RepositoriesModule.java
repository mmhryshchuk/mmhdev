package com.mmhdev.privatechat.core.di.modules;



import com.mmhdev.privatechat.core.android.App;
import com.mmhdev.privatechat.domain.repository.PreferenceRepository;
import com.mmhdev.privatechat.domain.repository.SaveEntityRepository;
import com.mmhdev.privatechat.domain.repository.impl.PreferenceRepositoryImpl;
import com.mmhdev.privatechat.domain.repository.impl.SaveEntityRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Created by  on 02.06.16.
 */
@Module(includes = {
        DataModule.class,
        MappersModule.class
})
public class RepositoriesModule {

    @Provides
    @Singleton
    PreferenceRepository providePreferenceRepository(App app){
        return new PreferenceRepositoryImpl(app);
    }
    @Provides
    @Singleton
    SaveEntityRepository provideSaveEntityRepo(
       /*     DaoSession daoSession,
            CatalogueMapper catalogueMapper,
            PurchaseIssueMapper issueMapper,
            SubscriptionMapper subscriptionMapper*/
    ){
        return new SaveEntityRepositoryImpl(
              /*  daoSession,
                catalogueMapper,
                issueMapper,
                subscriptionMapper*/
        );
    }


}
