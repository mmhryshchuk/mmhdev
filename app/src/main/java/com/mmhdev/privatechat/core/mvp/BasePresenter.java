package com.mmhdev.privatechat.core.mvp;

import android.os.Bundle;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by vladimir on 04.06.16.
 */
public abstract class BasePresenter<V> implements Presenter<V> {
    private V mView;
    private CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    @Override
    public void onRestore(Bundle savedInstanceState) {

    }

    public void onSaveInstance(Bundle output){

    }

    @Override
    public final void attachView(V view) {
        mView = view;
        onViewAttached();
    }

    @Override
    public final void detachView() {
        onViewDetached();
        mView = null;
        if (mCompositeSubscription != null){
            mCompositeSubscription.unsubscribe();
            mCompositeSubscription = new CompositeSubscription();
        }
    }

    protected void onViewAttached(){

    }

    protected void onViewDetached(){

    }

    protected void addSubscription(Subscription subscriber){
        mCompositeSubscription.add(subscriber);
    }

    protected void cleanSubscriptions(){
        if (mCompositeSubscription.isUnsubscribed()){
            mCompositeSubscription.unsubscribe();
        }
        mCompositeSubscription = new CompositeSubscription();
    }

    public final V getView() {
        return mView;
    }
}
