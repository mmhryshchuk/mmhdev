package com.mmhdev.privatechat.core.di.modules;



import com.mmhdev.privatechat.core.executors.PostExecutionThread;
import com.mmhdev.privatechat.core.executors.ThreadExecutor;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by  on 02.06.16.
 */
@Module
public class ThreadExecutorsModule {

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecuter(){
        return new ThreadExecutor.DefaultWorker();
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread (){
        return new PostExecutionThread.DefaultWorker();
    }


}
