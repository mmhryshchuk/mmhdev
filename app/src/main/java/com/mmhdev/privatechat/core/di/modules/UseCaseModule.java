package com.mmhdev.privatechat.core.di.modules;

import com.mmhdev.privatechat.core.android.AuthRetrofit;
import com.mmhdev.privatechat.core.executors.PostExecutionThread;
import com.mmhdev.privatechat.core.executors.ThreadExecutor;
import com.mmhdev.privatechat.domain.repository.PreferenceRepository;
import com.mmhdev.privatechat.domain.repository.SaveEntityRepository;
import com.mmhdev.privatechat.domain.use_cases.AuthUseCase;
import com.mmhdev.privatechat.domain.use_cases.impl.AuthUseCaseImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by vladimir on 02.06.16.
 */
@Module(includes = {
        RepositoriesModule.class,
        ThreadExecutorsModule.class,
        ApiModule.class
})
public class UseCaseModule {

   /* @Provides
    @Singleton
    MagazineUseCase provideMagazineUseCase(
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread,
            SaveEntityRepository saveEntityRepository,
            PreferenceRepository preferenceRepository,
            CatalogueRepository catalogueRepository,
            AuthRetrofit authRetrofit,
            DaoSession daoSession
    ){
        return new MagazineUseCaseImpl(daoSession, threadExecutor, preferenceRepository, postExecutionThread, saveEntityRepository, authRetrofit, catalogueRepository);
    }*/

    @Provides
    @Singleton
    AuthUseCase provideAuthUseCase(
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread,
            SaveEntityRepository saveEntityRepository,
            PreferenceRepository preferenceRepository,
            Retrofit retrofit,
            AuthRetrofit authRetrofit /*,
            DaoSession daoSession,
            ErrorMapper errorMapper*/
    ){
        return new AuthUseCaseImpl(
               /* threadExecutor,
                postExecutionThread,
                retrofit,
                authRetrofit,
                preferenceRepository,
                saveEntityRepository,
                daoSession,
                errorMapper*/
        );
    }

}
