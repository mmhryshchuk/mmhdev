package com.mmhdev.privatechat.core.mvp;

import android.os.Bundle;

/**
 * Created by vladimir on 04.06.16.
 */
public interface Presenter<V> {
    void onRestore(Bundle savedInstanceState);
    void onSaveInstance(Bundle outputState);
    void attachView(V view);
    void detachView();
    V getView();
}
