package com.mmhdev.privatechat.core.android;

import android.app.Application;
import android.content.Context;


import com.crashlytics.android.Crashlytics;
import com.mmhdev.privatechat.core.di.components.AppComponent;
import com.mmhdev.privatechat.core.di.components.DaggerAppComponent;
import com.mmhdev.privatechat.core.di.modules.ApiModule;
import com.mmhdev.privatechat.core.di.modules.AppModule;
import com.mmhdev.privatechat.core.di.modules.DataModule;
import com.mmhdev.privatechat.core.di.modules.MappersModule;
import com.mmhdev.privatechat.core.di.modules.ThreadExecutorsModule;

import io.fabric.sdk.android.Fabric;
import net.danlew.android.joda.JodaTimeAndroid;


/**
 * Created by vladimir on 25.07.16.
 */
public class App extends Application{

    private AppComponent appComponent;

    public static App getApp(Context context) {
        return (App) context.getApplicationContext();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//             This process is dedicated to LeakCanary for heap analysis.
//             You should not init your app in this process.
//            return;
//        }
//        LeakCanary.install(this);
//        Fabric.with(this, new Crashlytics());
        JodaTimeAndroid.init(this);
        setupAppComponent();
        setupPicasso();
    }

    private void setupAppComponent(){
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule())
                .threadExecutorsModule(new ThreadExecutorsModule())
                .dataModule(new DataModule())
                .mappersModule(new MappersModule())
                .build();
    }

    private void setupPicasso(){

    }
}
