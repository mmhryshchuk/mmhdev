package com.mmhdev.privatechat.core.utils;

import android.content.Context;

import com.mmhdev.privatechat.R;


/**
 * Created by on 28.10.16.
 */

public final class LandscapeUtils  {

    public static boolean isTablet(Context context){
        return context.getResources().getBoolean(R.bool.isTablet);
    }

}
