package com.mmhdev.privatechat.core.callbacks;

/**
 * Created by on 10.10.16.
 */

public interface OnItemLongClickListener<Data> {
    void onLongClicked(Data data);
}
